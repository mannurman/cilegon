<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Pangan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('komoditas_model');
        $this->load->model('jenis_komoditas_model');
        $this->load->model('jenis_pasar_model');
        $this->load->model('setting_model');
        $this->load->model('pasar_model');
    }

	public function index()
	{
        $jenis_komoditas = $this->jenis_komoditas_model->getAllRecursive();
        $pasar = $this->pasar_model->getAll();
        $jenis_pasar = $this->jenis_pasar_model->getAll();

        $parent_komoditas = $this->komoditas_model->parentKomoditas()['data'];

        $data_chart = json_encode($this->komoditas_model->parentKomoditas()['data_chart']);

		$this->load_view_frontend('frontend/beranda', compact(
		    'jenis_komoditas',
            'pasar',
            'jenis_pasar',
            'parent_komoditas',
            'data_chart'
        ));
	}

    public function informasi_harga($jenis_harga, $id_jenis_pasar)
    {
        $data = $this->komoditas_model->list_all_komoditas($id_jenis_pasar);

        if (!empty($this->input->get('submit'))) {
            if ($this->input->get('submit') == 'excel') {
                $this->download_excel($data);
            }
        }

        $pasar = $this->pasar_model->getAll();
        $jenis_komoditas = $this->jenis_komoditas_model->getAllRecursive();

        $jenis_pasar = $this->jenis_pasar_model->getById($id_jenis_pasar);

        $this->load_view_frontend('frontend/informasi_harga/index', compact(
            'jenis_harga',
            'data',
            'pasar',
            'jenis_pasar',
            'jenis_komoditas'
        ));
    }

	public function faq()
	{
	    $description = $this->setting_model->getByKey('SYARAT_KETENTUAN');

		$this->load_view_frontend('frontend/faq', compact('description'));
	}

	public function petunjuk()
	{
        $description = $this->setting_model->getByKey('PETUNJUK');

		$this->load_view_frontend('frontend/petunjuk', compact('description'));
	}

    // Export ke excel
    public function download_excel($data)
    {
        $spreadsheet = new Spreadsheet();

        $spreadsheet->getProperties()->setCreator('Andoyo - Java Web Media')
            ->setLastModifiedBy('Andoyo - Java Web Medi')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', $data['header']['penomoran'])
            ->setCellValue('B1', $data['header']['nama']);
        $col = 'C';
        foreach ($data['header']['tanggal_real'] as $val) {
            $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue($col.'1', $val);
            $col++;
        }

        $i = 2;
        foreach ($data['body'] as $value) {
            $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $value['penomoran_real'])
                ->setCellValue('B'.$i, $value['nama_real']);

            $col2 = 'C';
            foreach ($value['data_real'] as $val) {
                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue($col2.$i, $val);
                $col2++;
            }
            $i++;
        }

        $spreadsheet->getActiveSheet()->setTitle('Report Pangan '.date('d-m-Y H'));
        $spreadsheet->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Report Excel.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }
}
?>