<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once("application/core/MY_Backend_Controller.php");

class Jenis_komoditas extends MY_Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('jenis_komoditas_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $model = $this->jenis_komoditas_model->getAllNotRecursive();

        $this->load_view_backend('backend/jenis_komoditas/index', compact('model'));
    }

    public function add()
    {
        $jenis_komoditas = $this->jenis_komoditas_model;
        $validation = $this->form_validation;
        $validation->set_rules($jenis_komoditas->rules());

        if ($validation->run()) {
            $jenis_komoditas->save();
            redirect(site_url('backend/jenis_komoditas'));
        }

        $parent = $jenis_komoditas->getAllParent();

        $this->load_view_backend('backend/jenis_komoditas/create', compact('parent'));
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/products');

        $jenis_komoditas = $this->jenis_komoditas_model;
        $validation = $this->form_validation;
        $validation->set_rules($jenis_komoditas->rules());

        if ($validation->run()) {
            $jenis_komoditas->update($id);
            redirect(site_url('backend/jenis_komoditas'));
        }

        $model = $jenis_komoditas->getById($id);

        $parent = $jenis_komoditas->getAllParent();

        $this->load_view_backend('backend/jenis_komoditas/edit', compact('model', 'parent'));
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->jenis_komoditas_model->delete($id)) {
            redirect(site_url('backend/jenis_komoditas'));
        }
    }
}