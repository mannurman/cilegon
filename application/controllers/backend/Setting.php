<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once("application/core/MY_Backend_Controller.php");

class Setting extends MY_Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('setting_model');
    }

    public function index()
    {
        $settings = $this->setting_model->getAll();

        $this->load_view_backend('backend/settings/index', compact('settings'));
    }

    public function add()
    {
        $this->setting_model->save();

        redirect(site_url('backend/setting/index'));
    }
}