<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once("application/core/MY_Backend_Controller.php");

class Pasar extends MY_Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('pasar_model');
        $this->load->model('jenis_pasar_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $pasar = $this->pasar_model->getAll();

        $jenis_pasar = $this->jenis_pasar_model->getAll();

        $this->load_view_backend('backend/data_pasar/data_pasar', compact('pasar', 'jenis_pasar'));
    }

    public function add()
    {
        $product = $this->pasar_model;
        $validation = $this->form_validation;
        $validation->set_rules($product->rules());

        if ($validation->run()) {
            $product->save();
//            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect(site_url('backend/pasar'));
        }

        $jenis_pasar = $this->jenis_pasar_model->getAll();

        $this->load_view_backend('backend/data_pasar/tambah_data_pasar', compact('jenis_pasar'));
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/products');

        $product = $this->pasar_model;
        $validation = $this->form_validation;
        $validation->set_rules($product->rules());

        if ($validation->run()) {
            $product->update($id);
//            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect(site_url('backend/pasar'));
        }

        $pasar = $product->getById($id);

        $jenis_pasar = $this->jenis_pasar_model->getAll();

        $this->load_view_backend('backend/data_pasar/edit_data_pasar', compact('pasar', 'jenis_pasar'));
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->pasar_model->delete($id)) {
            redirect(site_url('backend/pasar'));
        }
    }
}