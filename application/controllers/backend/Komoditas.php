<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once("application/core/MY_Backend_Controller.php");

class Komoditas extends MY_Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('jenis_komoditas_model');
        $this->load->model('pasar_model');
        $this->load->model('komoditas_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data = $this->komoditas_model->list_all_komoditas();

        $pasar = $this->pasar_model->getAll();
        $jenis_komoditas = $this->jenis_komoditas_model->getAllRecursive();

        $this->load_view_backend('backend/komoditas/komoditas', compact(
            'data',
            'pasar',
            'jenis_komoditas'
        ));
    }

    public function tambah_komoditas()
    {
        $jenis_komoditas = $this->jenis_komoditas_model->getAllNotRecursive();
        $pasar = $this->pasar_model->getAll();

        $komoditas = $this->komoditas_model;
        $validation = $this->form_validation;
        $validation->set_rules($komoditas->rules());

        if ($validation->run()) {
            $komoditas->save();
//            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect(site_url('backend/komoditas'));
        }

        $this->load_view_backend('backend/komoditas/tambah_komoditas', compact('jenis_komoditas', 'pasar'));
    }

    public function get_komoditas()
    {
        $tanggal    = $this->input->post('tanggal');
        $pasar_id   = $this->input->post('pasar_id');

        $data  = $this->komoditas_model->getKomoditas($tanggal, $pasar_id);

        $status['data'] = $data;
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($status, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }
}