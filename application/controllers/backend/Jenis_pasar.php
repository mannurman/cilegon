<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once("application/core/MY_Backend_Controller.php");

class Jenis_pasar extends MY_Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('jenis_pasar_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $model = $this->jenis_pasar_model->getAll();

        $this->load_view_backend('backend/jenis_pasar/index', compact('model'));
    }

    public function add()
    {
        $jenis_pasar = $this->jenis_pasar_model;
        $validation = $this->form_validation;
        $validation->set_rules($jenis_pasar->rules());

        if ($validation->run()) {
            $jenis_pasar->save();
            redirect(site_url('backend/jenis_pasar'));
        }

        $this->load_view_backend('backend/jenis_pasar/create');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/products');

        $jenis_pasar = $this->jenis_pasar_model;
        $validation = $this->form_validation;
        $validation->set_rules($jenis_pasar->rules());

        if ($validation->run()) {
            $jenis_pasar->update($id);
            redirect(site_url('backend/jenis_pasar'));
        }

        $model = $jenis_pasar->getById($id);

        $this->load_view_backend('backend/jenis_pasar/edit', compact('model'));
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->jenis_pasar_model->delete($id)) {
            redirect(site_url('backend/jenis_pasar'));
        }
    }
}