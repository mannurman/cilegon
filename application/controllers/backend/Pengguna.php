<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once("application/core/MY_Backend_Controller.php");

class Pengguna extends MY_Backend_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $model = $this->user_model->getAll();

        $this->load_view_backend('backend/pengguna/index', compact('model'));
    }

    public function add()
    {
        $pengguna = $this->user_model;
        $validation = $this->form_validation;
        $validation->set_rules($pengguna->rules());

        if ($validation->run()) {
            $pengguna->save();
//            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect(site_url('backend/pengguna'));
        }

        $this->load_view_backend('backend/pengguna/create');
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/products');
        $pengguna = $this->user_model;
        $validation = $this->form_validation;
        $validation->set_rules($pengguna->rules_edit());

        if ($validation->run()) {
            $pengguna->update($id);
//            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect(site_url('backend/pengguna'));
        }

        $model = $pengguna->getById($id);

        $this->load_view_backend('backend/pengguna/edit', compact('model'));
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();

        if ($this->user_model->delete($id)) {
            redirect(site_url('backend/pengguna'));
        }
    }

    public function check_user_username($username) {
        if($this->input->post('id'))
            $id = $this->input->post('id');
        else
            $id = '';

        $result = $this->user_model->check_unique_user_username($id, $username);
        if($result == 0)
            $response = true;
        else {
            $this->form_validation->set_message('check_user_username', 'Username must be unique');
            $response = false;
        }
        return $response;
    }
}