<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once("application/core/MY_Backend_Controller.php");

class Admin extends MY_Backend_Controller {

	public function index()
	{
		$this->load_view_backend('backend/beranda');
	}
}
