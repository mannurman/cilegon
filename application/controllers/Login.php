<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->library('session');
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $where = array(
            'username' => $username,
            'password' => md5($password)
        );
        $cek = $this->user_model->cek_login($where)->row();

        if($cek){
            $data_session = array(
                'nama' => $username,
                'status' => 'login',
                'role' => $cek->level
            );

            $this->session->set_userdata($data_session);

            $status['status'] = true;
        }else{
            $status['status'] = false;
        }

        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($status, JSON_PRETTY_PRINT))
            ->_display();
        exit;
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url('/'));
    }
}