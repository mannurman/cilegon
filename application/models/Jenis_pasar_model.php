<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_pasar_model extends CI_Model
{
    private $_table = "jenis_pasar";

    public $nama;

    public function rules()
    {
        return [
            ['field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'],
        ];
    }

    public function getAll()
    {
        $query = $this->db;

        if (!empty($this->input->get('search'))) {
            $this->db->like('nama', $this->input->get('search'));
        }

        return $query->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();

        $this->nama         = $post["nama"];
        $this->created_at   = date('Y-m-d h:i:s');

        $this->db->insert($this->_table, $this);
    }

    public function update($id)
    {
        $post = $this->input->post();

        $this->nama         = $post["nama"];
        $this->updated_at   = date('Y-m-d h:i:s');

        $this->db->update($this->_table, $this, ['id' => $id]);
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, ["id" => $id]);
    }
}