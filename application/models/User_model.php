<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $_table = "users";

    public $name;
    public $username;
    public $password;
    public $level;
    public $created_at;
    public $updated_at;

    public function rules()
    {
        return [
            ['field' => 'username',
                'label' => 'Username',
                'rules' => 'required|is_unique[users.username]'],
            ['field' => 'name',
                'label' => 'Name',
                'rules' => 'required'],
            ['field' => 'password',
                'label' => 'Password',
                'rules' => 'required'],

            ['field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[password]'],
        ];
    }

    public function rules_edit()
    {
        return [
            ['field' => 'username',
                'label' => 'Username',
                'rules' => 'required|callback_check_user_username'],
            ['field' => 'name',
                'label' => 'Name',
                'rules' => 'required'],
        ];
    }

    public function getAll()
    {
        $query = $this->db;

        if (!empty($this->input->get('name'))) {
            $this->db->like('name', $this->input->get('name'));
        }

        if (!empty($this->input->get('username'))) {
            $this->db->like('username', $this->input->get('username'));
        }

        if (!empty($this->input->get('level'))) {
            $query->where('level', $this->input->get('level'));
        }

        return $query->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();

        $this->name         = $post["name"];
        $this->username     = $post["username"];
        $this->password     = md5($post['password']);
        $this->level        = $post['level'];
        $this->created_at   = date('Y-m-d h:i:s');

        $this->db->insert($this->_table, $this);
    }

    public function update($id)
    {
        $post = $this->input->post();

        $data['name']         = $post["name"];
        $data['username']     = $post["username"];
        if (!empty($post['password'])) {
            $data['password'] = md5($post['password']);
        } else {
            $user = $this->getById($id);
            $data['password'] = $user->password;
        }
        $data['level']        = $post['level'];
        $data['created_at']   = date('Y-m-d h:i:s');

        $this->db->update($this->_table, $data, ['id' => $id]);
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, ["id" => $id]);
    }

    public function cek_login($where){
        return $this->db->get_where($this->_table, $where);
    }

    public function check_unique_user_username($id = '', $username) {
        $this->db->where('username', $username);
        if($id) {
            $this->db->where_not_in('id', $id);
        }
        return $this->db->get($this->_table)->num_rows();
    }
}