<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Komoditas_model extends CI_Model
{
    private $_table = "komoditas";

    public $tanggal;
    public $pasar_id;
    public $jenis_komoditas_id;
    public $harga;

    public function __construct()
    {
        $this->load->helper('new_helper');
    }

    public function rules()
    {
        return [
            ['field' => 'tanggal',
                'label' => 'Tanggal',
                'rules' => 'required'],

            ['field' => 'pasar_id',
                'label' => 'Pasar',
                'rules' => 'required'],
        ];
    }

    public function save()
    {
        $post = $this->input->post();

        $this->db->delete($this->_table, [
            "pasar_id" => $post["pasar_id"],
            'tanggal' => \Carbon\Carbon::parse($post["tanggal"])->format('Y-m-d')
        ]);

        foreach ($post['harga'] as $key => $harga) {
            $this->tanggal            = \Carbon\Carbon::parse($post["tanggal"])->format('Y-m-d');
            $this->pasar_id           = $post["pasar_id"];
            $this->jenis_komoditas_id = $key;
            $this->harga              = !empty($harga) ? $harga : 0;
            $this->created_at         = date('Y-m-d h:i:s');

            $this->db->insert($this->_table, $this);
        }

        return true;
    }

    public function parentKomoditas()
    {
        $CI =& get_instance();
        $CI->load->model('jenis_komoditas_model');

        $list_komoditas = $CI->jenis_komoditas_model->getAllParent();

        $data = [];
        $i = 1;
        $data_chart = [];
        foreach ($list_komoditas as $value) {
            $data[] = [
                'nomor' => numberToRomanRepresentation($i),
                'nama' => $value->nama,
                'harga' => $this->parentHarga($value->id)
            ];
            $data_chart[] = [
                'label' => $value->nama,
                'y' => (int) str_replace('.', '', $this->parentHarga($value->id))
            ];
            $i++;
        }

        return [
            'data' => $data,
            'data_chart' => $data_chart
        ];
    }

    public function list_all_komoditas($jenis_pasar = null)
    {
        $CI =& get_instance();
        $CI->load->model('jenis_komoditas_model');

        $list_komoditas = $CI->jenis_komoditas_model->getAllNotRecursive();

        $get_data = $this->input->get();

        $url_next = '?page=1';
        $url_prev = '?page=0';
        if (!empty($get_data)) {
            $url_next = $this->getUrl($get_data, true);
            $url_prev = $this->getUrl($get_data, false);
        }

        $detail = $this->getDetail($get_data);

        $header = [];
        $first = true;
        $data = [];
        foreach ($list_komoditas as $value) {
            if (!empty($get_data['komoditas_ids'])) {
                if (!in_array($value->id, $get_data['komoditas_ids']))
                    continue;
            }

            $data_tanggal = $this->list_komoditas($value->id, $value->parent_id, $jenis_pasar);
            if ($first) {
                $header = [
                    'penomoran'     => 'NO.',
                    'nama'          => 'Komoditas (Rp)',
                    'tanggal'       => $data_tanggal['tanggal'],
                    'tanggal_real'  => $data_tanggal['tanggal_real'],
                ];
                $first = false;
            }

            $data[] = [
                'penomoran' => $this->isStrongText($value->penomoran, $value->parent_id),
                'penomoran_real' => $value->penomoran,
                'nama'      => $this->isStrongText($value->nama, $value->parent_id),
                'nama_real' => $value->nama,
                'em'        => is_null($value->parent_id) ? 0 : 1,
                'data'      => $data_tanggal['harga'],
                'data_real' => $data_tanggal['harga_real'],
            ];
        }

        return [
            'header' => $header,
            'body'  => $data,
            'detail' => $detail,
            'url_next' => $url_next,
            'url_prev' => $url_prev,
        ];
    }

    public function list_komoditas($jenis_komoditas_id, $parent_id, $jenis_pasar)
    {
        $attributes = $this->input->get();
        $offset = 0;
        if (!empty($attributes['page'])) {
            $offset = ((int)$attributes['page'] * 6);
        }

        if (!empty($attributes['tipe_laporan'])) {
            if ($attributes['tipe_laporan'] == 'weekly') {
                $query = $this->queryWeeks($jenis_komoditas_id);
            } elseif ($attributes['tipe_laporan'] == 'monthly') {
                $query = $this->queryMonts($jenis_komoditas_id);
            } else {
                $query = $this->queryDays($jenis_komoditas_id);
            }
        } else {
            $query = $this->queryDays($jenis_komoditas_id);
        }

        if ($jenis_pasar) {
            $query->join('pasar', 'pasar.id = komoditas.pasar_id')
            ->where('pasar.jenis_pasar_id', $jenis_pasar);
        }

        if (!empty($attributes['pasar_ids'])) {
            $query->where_in('pasar_id', $attributes['pasar_ids']);
        }

        if (!empty($attributes['start_date'])) {
            $this->db->where('tanggal >=', $attributes['start_date']);
        }

        if (!empty($attributes['end_date'])) {
            $this->db->where('tanggal <=', $attributes['end_date']);
        }

        $komoditas = $query
            ->limit(6, $offset)
            ->get($this->_table)
            ->result();

        $tanggal_real = [];
        $tanggal      = [];
        $harga        = [];
        $harga_real   = [];
        foreach ($komoditas as $value) {
            $tanggal[]      = $this->isStrongText($value->tanggal, $parent_id);
            $tanggal_real[] = $value->tanggal;
            $harga[]        = $this->isStrongText(number_format($value->harga_rata_rata, 0, '', '.'), $parent_id);
            $harga_real[]   = number_format($value->harga_rata_rata, 0, '', '.');
        }

        return [
            'tanggal'       => $tanggal,
            'tanggal_real'  => $tanggal_real,
            'harga'         => $harga,
            'harga_real'    => $harga_real,
        ];
    }

    public function queryDays($jenis_komoditas_id)
    {
        $query = $this->db->select('jenis_komoditas_id, tanggal, avg(harga) as harga_rata_rata')
            ->where('jenis_komoditas_id', $jenis_komoditas_id)
            ->group_by('tanggal, jenis_komoditas_id')
            ->order_by('tanggal', 'DESC');

        return $query;
    }

    public function queryWeeks($jenis_komoditas_id)
    {
        $query = $this->db->select('jenis_komoditas_id, 
            CONCAT(YEAR(tanggal),\'/\',MONTH(tanggal),\'/\',FLOOR((DayOfMonth(tanggal)-1)/7)+1) as tanggal,
            avg(harga) as harga_rata_rata')
            ->where('jenis_komoditas_id', $jenis_komoditas_id)
            ->group_by('CONCAT(YEAR(tanggal),\'/\',MONTH(tanggal),\'/\',FLOOR((DayOfMonth(tanggal)-1)/7)+1), jenis_komoditas_id')
            ->order_by('CONCAT(YEAR(tanggal),\'/\',MONTH(tanggal),\'/\',FLOOR((DayOfMonth(tanggal)-1)/7)+1) DESC');

        return $query;
    }

    public function queryMonts($jenis_komoditas_id)
    {
        $query = $this->db->select('jenis_komoditas_id, CONCAT(YEAR(tanggal),\'/\',MONTH(tanggal)) as tanggal, avg(harga) as harga_rata_rata')
            ->where('jenis_komoditas_id', $jenis_komoditas_id)
            ->group_by('CONCAT(YEAR(tanggal),\'/\',MONTH(tanggal)), jenis_komoditas_id')
            ->order_by('CONCAT(YEAR(tanggal),\'/\',MONTH(tanggal)) DESC');

        return $query;
    }

    public function parentHarga($jenis_komoditas_id)
    {
        $CI =& get_instance();
        $CI->load->model('jenis_komoditas_model');

        $ids = $CI->jenis_komoditas_model->idParentChild($jenis_komoditas_id);


        $query = $this->db->select('avg(harga) as harga_rata_rata')
            ->where_in('jenis_komoditas_id', $ids);

        if (!empty($this->input->get('komoditas_id'))) {
            $query->where('jenis_komoditas_id', $this->input->get('komoditas_id'));
        }

        if (!empty($this->input->get('jenis_pasar_id'))) {
            $query->join('pasar', 'pasar.id = komoditas.pasar_id')
                ->where('pasar.jenis_pasar_id', $this->input->get('jenis_pasar_id'));
        }

        if (!empty($this->input->get('pasar_id'))) {
            $query->where('pasar_id', $this->input->get('pasar_id'));
        }

        if (!empty($this->input->get('tanggal'))) {
            $query->where('tanggal', \Carbon\Carbon::parse($this->input->get('tanggal'))->format('Y-m-d'));
        }

       $query = $query->get($this->_table)->row();

       if (!empty($query)) {
           return number_format($query->harga_rata_rata, 0, '', '.');
       }

       return 0;
    }

    public function getUrl($data, $is_next = true)
    {
        $page['page'] = 0;
        if ($is_next) {
            $page['page'] = $data['page'] + 1;
        } elseif (!$is_next and !empty($data['page'])) {
            $page['page'] = $data['page'] - 1;
        }

        $data = array_merge($data, $page);

        return '?'.http_build_query($data);
    }

    public function isStrongText($value, $parent_id)
    {
        if (is_null($parent_id)) {
            $value = '<strong>'.$value.'</strong>';
        }

        return $value;
    }

    public function getKomoditas($tanggal, $pasar_id)
    {
        $tanggal = \Carbon\Carbon::parse($tanggal)->format('Y-m-d');
        $data = $this->db->where('tanggal', $tanggal)->where('pasar_id', $pasar_id)
            ->get($this->_table)->result_array();

        return $data;
    }

    public function getDetail($attributes)
    {
        $period = '';
        if (!empty($attributes['start_date']) || !empty($attributes['end_date'])) {
            $period = $attributes['start_date'].' - '.$attributes['end_date'];
        }

        $pasar = 'Semua Pasar';
        if (!empty($attributes['pasar_ids'])) {

            $CI =& get_instance();
            $CI->load->model('pasar_model');

            $data = $CI->pasar_model->getByIds($attributes['pasar_ids']);

            $data = array_pluck($data, 'nama');

            $pasar = implode(', ', $data);
        }

        $tipe_laporan = 'Laporan Harian';
        if (!empty($attributes['tipe_laporan'])) {
            $tipe_laporan = tipeLaporan($attributes['tipe_laporan']);
        }

        return [
            'period' => $period,
            'pasar'  => $pasar,
            'tipe_laporan' => $tipe_laporan
        ];
    }
}