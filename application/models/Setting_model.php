<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model
{
    private $_table = "settings";

    public $key;
    public $value;

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function save()
    {
        $post = $this->input->post();

        if (!empty($post['settings'])) {
            foreach ($post['settings'] as $key => $value) {
                $this->value = $value;
                $this->key   = $key;

                $this->db->update($this->_table, $this, ['key' => $key]);
            }
        }
    }

    public function getByKey($key)
    {
        return $this->db->get_where($this->_table, ["key" => $key])->row()->value;
    }
}