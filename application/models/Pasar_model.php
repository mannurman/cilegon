<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pasar_model extends CI_Model
{
    private $_table = "pasar";

    public $nama;
    public $jenis_pasar_id;
    public $latitude;
    public $longitude;
    public $alamat_pasar;
    public $created_at;
    public $updated_at;

    public function rules()
    {
        return [
            ['field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'],

            ['field' => 'jenis_pasar_id',
                'label' => 'Jenis Pasar',
                'rules' => 'required'],
        ];
    }

    public function getAll()
    {
        $query = $this->db;
        $query->select('pasar.id, pasar.nama, jenis_pasar.nama as jenis_pasar_nama, alamat_pasar');
        if (!empty($this->input->get('nama'))) {
            $this->db->like('nama', $this->input->get('nama'));
        }

        if (!empty($this->input->get('alamat_pasar'))) {
            $this->db->like('alamat_pasar', $this->input->get('alamat_pasar'));
        }

        if (!empty($this->input->get('jenis_pasar_id'))) {
            $query->where('jenis_pasar_id', $this->input->get('jenis_pasar_id'));
        }

        $query->join('jenis_pasar', 'pasar.jenis_pasar_id = jenis_pasar.id');

        return $query->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();

        $this->jenis_pasar_id  = $post["jenis_pasar_id"];
        $this->nama         = $post["nama"];
        $this->latitude     = $post["latitude"];
        $this->longitude    = $post["longitude"];
        $this->alamat_pasar = $post["alamat_pasar"];
        $this->created_at   = date('Y-m-d h:i:s');

        $this->db->insert($this->_table, $this);
    }

    public function update($id)
    {
        $post = $this->input->post();

        $this->jenis_pasar_id  = $post["jenis_pasar_id"];
        $this->nama         = $post["nama"];
        $this->latitude     = $post["latitude"];
        $this->longitude    = $post["longitude"];
        $this->alamat_pasar = $post["alamat_pasar"];
        $this->updated_at   = date('Y-m-d h:i:s');

        $this->db->update($this->_table, $this, ['id' => $id]);
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, ["id" => $id]);
    }

    public function getByIds($ids)
    {
        return $this->db->where_in('id', $ids)->get($this->_table)->result_array();
    }
}