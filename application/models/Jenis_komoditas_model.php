<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_komoditas_model extends CI_Model
{
    private $_table = "jenis_komoditas";

    public $nama;
    public $parent_id;
    public $created_at;
    public $updated_at;

    public function __construct()
    {
        $this->load->helper('new_helper');
    }

    public function rules()
    {
        return [
            ['field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'],
        ];
    }

    public function getAll()
    {
        return $this->db
            ->select('jenis_komoditas.id, jenis_komoditas.nama, parent.nama as parent')
            ->join('jenis_komoditas as parent', 'parent.id = jenis_komoditas.parent_id', 'left')
            ->get($this->_table)->result();
    }

    public function save()
    {
        $post = $this->input->post();

        $this->nama         = $post["nama"];
        $this->parent_id    = $post["parent_id"];
        $this->created_at   = date('Y-m-d h:i:s');

        $this->db->insert($this->_table, $this);
    }

    public function update($id)
    {
        $post = $this->input->post();

        $this->nama         = $post["nama"];
        $this->parent_id    = $post["parent_id"];
        $this->updated_at   = date('Y-m-d h:i:s');

        $this->db->update($this->_table, $this, ['id' => $id]);
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, ["id" => $id]);
    }

    public function getAllParent()
    {
        return $this->db->where('parent_id is null')->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function getAllNotRecursive()
    {
        $recursive = $this->getAllRecursive();

        $data = [];
        $i = 1;
        foreach ($recursive as $value) {
            $data[] = (object) [
                'id'        => $value['id'],
                'parent_id' => $value['parent_id'],
                'penomoran' => numberToRomanRepresentation($i),
                'nama'      => $value['nama'],
                'parent'    => $value['parent'],
            ];
            $i2 = 1;
            foreach ($value['children'] as $child) {
                $data[] = (object) [
                    'id'        => $child['id'],
                    'parent_id' => $child['parent_id'],
                    'penomoran' => $i2,
                    'nama'      => $child['nama'],
                    'parent'    => $child['parent'],
                ];
                $i2++;
            }
            $i++;
        }

        return $data;
    }

    public function getAllRecursive()
    {
        $query = $this->db->select('jenis_komoditas.id, jenis_komoditas.nama, parent.nama as parent, jenis_komoditas.parent_id')
            ->join('jenis_komoditas as parent', 'parent.id = jenis_komoditas.parent_id', 'left');

        if (!empty($this->input->get('search'))) {
            $this->db->like('jenis_komoditas.nama', $this->input->get('search'));
        }

        $query = $query->get($this->_table)->result_array();

        return $this->buildTree($query);
    }

    public function buildTree(array $elements, $parentId = 0)
    {
        $branch = [];
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = array_merge($element);
            }
        }

        return $branch;
    }

    public function idParentChild($id)
    {
        $data = $this->db->select('id')
            ->where('parent_id', $id)->or_where('id', $id)
            ->get($this->_table)->result();
        $ids = [];
        foreach ($data as $val) {
            $ids[] = $val->id;
        }

       return $ids;
    }
}