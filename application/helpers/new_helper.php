<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('numberToRomanRepresentation'))
{
    function numberToRomanRepresentation($number) {
        $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $returnValue = '';
        while ($number > 0) {
            foreach ($map as $roman => $int) {
                if($number >= $int) {
                    $number -= $int;
                    $returnValue .= $roman;
                    break;
                }
            }
        }
        return $returnValue;
    }
}

if ( ! function_exists('settingName'))
{
    function settingName($key) {

        $data = [
            'SYARAT_KETENTUAN' => 'Syarat dan Ketentuan',
            'PETUNJUK'         => 'Petunjuk Aplikasi'
        ];

        return $data[$key];
    }
}

if ( ! function_exists('tipeLaporan'))
{
    function tipeLaporan($key) {

        $data = [
            'day'       => 'Laporan Harian',
            'weekly'    => 'Laporan Mingguan',
            'monthly'   => 'Laporan Bulanan',
        ];

        return $data[$key];
    }
}

if ( ! function_exists('array_pluck'))
{
    /**
     * Pluck an array of values from an array.
     *
     * @param  array   $array
     * @param  string  $value
     * @param  string  $key
     * @return array
     */
    function array_pluck($array, $key = null)
    {
        $data = [];
        foreach ($array as $value) {
            $data[] = $value[$key];
        }

        return $data;
    }
}