<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="id-id">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>e-Harga Pangan - Beranda</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico')?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css')?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/template.css')?>"/>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<style type="text/css">
	.container {width: 1124px !important;} .t3-wrapper, .wrap {min-width: 1124px !important;}
	</style>
</head>
<body class="home beranda">
	<div class="t3-wrapper">
		<nav id="t3-mainnav" class="wrap navbar navbar-default t3-mainnav">
			<div class="container">
				<div class="row">
					<div class="logo pull-left">
						<div class="logo-image">
							<a href="<?php echo base_url('')?>" title="e-Harga Pangan">
								<img class="logo-img" src="<?php echo base_url('assets/img/logo.png')?>" alt="e-Harga Pangan"/>
							</a>
							<small class="site-slogan">Pusat Informasi Harga Pangan Kota Cilegon</small>
						</div>
					</div>
				</div>
			</div>
		</nav>
		<div class="container t3-sl t3-sl-1">
			<center>Ooopss...<img src="<?php echo base_url('assets/img/404.png')?>"/><br><br><a href="<?php echo base_url('')?>"><button type="button" class="btn btn-danger">Kembali</button></a></center>
		</div>
		<footer id="t3-footer" class="wrap t3-footer">
			<section class="t3-copyright">
				<div class="container">
					<div class="copyright ">
						<div class="module">
							<small>Page rendered in <strong>{elapsed_time}</strong> seconds. &copy; 2019 eGov Pemerintah Kota Cilegon - Pusat Informasi Harga Pangan Pemerintah Kota Cilegon</small>
						</div>
					</div>
				</div>
			</section>
		</footer>
	</div>
</html>