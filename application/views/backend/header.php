<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/img/favicon.ico'); ?>" rel="stylesheet" type="text/css"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Dashboard Administrator</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="<?php echo base_url('/assets/css/bootstrap_admin.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('/assets/css/jquery-ui.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('/assets/css/animate.min.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('/assets/css/light-bootstrap-dashboard.css'); ?>" rel="stylesheet"/>
    <link href="<?php echo base_url('/assets/css/demo.css'); ?>" rel="stylesheet" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url('/assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('/assets/redactor/redactor.min.css'); ?>" rel="stylesheet" />
    <style>
        p {
            font-size: 13px !important;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="sidebar" data-color="blue">
    	<div class="sidebar-wrapper">
            <div class="logo">
				<img src="<?php echo base_url('/assets/img/pemkot.png'); ?>" width="230px" height="auto"/>
            </div>
            <ul class="nav">
                <li class="">
                    <a href="<?php echo base_url('admin'); ?>">
                        <i class="pe-7s-graph"></i>
                        <p>Beranda</p>
                    </a>
                </li>
                <?php if ($role == 'ADMIN') : ?>
                <li>
                    <a href="<?php echo base_url('backend/pasar'); ?>">
                        <i class="pe-7s-note2"></i>
                        <p>Data Pasar</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('backend/jenis_pasar'); ?>">
                        <i class="pe-7s-note2"></i>
                        <p>Data Jenis Pasar</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('backend/jenis_komoditas'); ?>">
                        <i class="pe-7s-note2"></i>
                        <p>Data Jenis Komoditas</p>
                    </a>
                </li>
                <?php endif; ?>
                <li>
                    <a href="<?php echo base_url('backend/komoditas'); ?>">
                        <i class="pe-7s-cart"></i>
                        <p>Komoditas</p>
                    </a>
                </li>
                <?php if ($role == 'ADMIN') : ?>
                <li>
                    <a href="<?php echo base_url('backend/pengguna'); ?>">
                        <i class="pe-7s-user"></i>
                        <p>Pengguna</p>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('backend/setting'); ?>">
                        <i class="pe-7s-settings"></i>
                        <p>Setting</p>
                    </a>
                </li>
                <?php endif; ?>
                <li>
                    <a href="<?php echo base_url('login/logout'); ?>">
                        <i class="pe-7s-power"></i>
                        <p>Logout</p>
                    </a>
                </li>
				<li class="active-pro">
                    <a href="">
                        <?php $ip = $_SERVER['REMOTE_ADDR'];?>
						<i class="pe-7s-share"></i>
                        <p>IP Anda <?php echo $ip; ?></p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
					<a class="navbar-brand" href="<?php echo base_url(); ?>">Dashboard Administrator e-Harga Pangan Pemerintah Kota Cilegon</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="<?php echo base_url('login/logout'); ?>">
                                <p>Logout</p>
                            </a>
                        </li>
						<li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>