<?php if(!empty(validation_errors())) : ?>
<div class="alert alert-danger" role="alert">
    <?= validation_errors() ?>
</div>
<?php endif; ?>
<input type="hidden" name="id" value="<?= $model->id ?? null ?>">
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Nama Pengguna</label></br>
            <input type="text" name="name" value="<?= $model->name ?? null ?>" class="form-control" placeholder="Nama Pengguna">
        </div>
        <div class="form-group">
            <label>Username</label></br>
            <input type="text" name="username" value="<?= $model->username ?? null ?>" class="form-control" placeholder="Username">
        </div>
        <div class="form-group">
            <label>Password</label></br>
            <input type="password" name="password" class="form-control" placeholder="Password">
        </div>
        <div class="form-group">
            <label>Ulangi Password</label></br>
            <input type="password" name="confirm_password" class="form-control" placeholder="Ulangi Password">
        </div>
        <div class="form-group">
            <label>Level</label></br>
            <select name="level" class="form-control">
                <option <?= !empty($model->level) ? ($model->level == 'ADMIN') ? 'selected' : null : null  ?> value="ADMIN">Administrator</option>
                <option <?= !empty($model->level) ? ($model->level == 'OPERATOR') ? 'selected' : null : null  ?> value="OPERATOR">Operator</option>
            </select>
        </div>
    </div>
</div>
<button type="submit" value="" class="btn btn-info btn-fill pull-left">Simpan Data</button>
<div class="clearfix"></div>