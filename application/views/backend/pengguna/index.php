<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Pengguna</h4>
                    </div>
                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Filtering</div>
                                        <div class="panel-body">
                                            <form action="" method="GET">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="name" value="<?= $_GET['name'] ?? null ?>" class="form-control" placeholder="Nama Pengguna">
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" name="username" value="<?= $_GET['username'] ?? null ?>" class="form-control" placeholder="Username">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <select name="level" class="form-control">
                                                                <option value="">Pilih Level</option>
                                                                <option <?= !empty($model->level) ? ($model->level == 'ADMIN') ? 'selected' : null : null  ?> value="ADMIN">Administrator</option>
                                                                <option <?= !empty($model->level) ? ($model->level == 'OPERATOR') ? 'selected' : null : null  ?> value="OPERATOR">Operator</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <button type="submit" class="btn btn-primary btn-fill pull-left">Search</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <a href="<?php echo base_url('backend/pengguna/add')?>"><button class="btn btn-info btn-fill pull-left">Tambah Pengguna</button></a>
                                    <div class="content table-responsive table-full-width">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                                <th>Nama Pengguna</th>
                                                <th>Username</th>
                                                <th>Level</th>
                                                <th>Aksi</th>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($model as $value) : ?>
                                                    <tr>
                                                        <td><?= $value->name ?></td>
                                                        <td><?= $value->username ?></td>
                                                        <td><?= $value->level ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url('backend/pengguna/edit/'. $value->id) ?>"><div class="fa fa-pencil"></div></a>
                                                            <a href="<?= base_url('backend/pengguna/delete/'. $value->id) ?>" onclick="return confirm('apa anda yakin?')"><div class="fa fa-trash"></div></a>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>