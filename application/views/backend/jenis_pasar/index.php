<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Data Jenis Pasar</h4>
                    </div>
                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="<?php echo base_url('backend/jenis_pasar/add')?>"><button class="btn btn-info btn-fill pull-left">Tambah Data</button></a>
                                    <form method="GET">
                                        <div class="input-group col-md-3 pull-right">
                                            <input type="text" name="search" value="<?= $_GET['search'] ?? null ?>" class="form-control" placeholder="Cari">
                                            <div class="input-group-btn">
                                                <button class="btn btn-outline-secondary" type="submit">Cari</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="content table-responsive table-full-width">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <th>Nama Pasar</th>
                                            <th>Aksi</th>
                                            </thead>
                                            <?php foreach ($model as $value) : ?>
                                                <tr>
                                                    <td><?= $value->nama ?></td>
                                                    <td>
                                                        <a href="<?php echo base_url('backend/jenis_pasar/edit/'. $value->id) ?>"><div class="fa fa-pencil"></div></a>
                                                        <a href="<?= base_url('backend/jenis_pasar/delete/'. $value->id) ?>" onclick="return confirm('apa anda yakin?')"><div class="fa fa-trash"></div></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>