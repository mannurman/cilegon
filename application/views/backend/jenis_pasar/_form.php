<div class="row">
    <div class="col-md-12">
        <div class="form-group <?php echo form_error('nama') ? 'has-error':'' ?>">
            <label>Nama</label></br>
            <input type="text" name="nama" value="<?= $model->nama ?? null ?>" class="form-control" placeholder="Nama Pasar">
            <div class="alert-error"><?php echo form_error('nama') ?></div>
        </div>
    </div>
</div>

<button type="submit" value="" class="btn btn-info btn-fill pull-left">Simpan Data</button>
<div class="clearfix"></div>