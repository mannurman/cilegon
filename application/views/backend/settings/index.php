<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Settings</h4>
                    </div>
                    <div class="content">
                        <form enctype="multipart/form-data" action="<?php echo base_url('backend/setting/add')?>" method="POST">
                            <div class="clearfix"></div>
                            <?php foreach ($settings as $value) : ?>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><?= settingName($value->key) ?></label></br>
                                    <textarea name="settings[<?= $value->key?>]" class="text-redactor"><?= $value->value?></textarea>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <button type="submit" value="" class="btn btn-info btn-fill pull-left">Simpan Data</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>