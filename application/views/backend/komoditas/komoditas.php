<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Komoditas</h4>
                    </div>
                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="report-header">
                                        <h4>Perkembangan Harga Pangan</h4>
                                        <div>
                                            <span>Periode</span> : <?= $data['detail']['period']?>
                                        </div>
                                        <div>
                                            <span>Pasar</span> : <?= $data['detail']['pasar']?>
                                        </div>
                                        <div>
                                            <span>Tipe Laporan</span> : <?= $data['detail']['tipe_laporan']?>
                                        </div>
                                    </div>
                                    <hr/>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">Filtering</div>
                                        <div class="panel-body">
                                            <form action="" method="GET">
                                                <input type="hidden" name="page" value="<?= $_GET['page'] ?? 0 ?>">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="formGroupExampleInput">Komoditas</label>
                                                            <select id="filter_commodity_ids" name="komoditas_ids[]" class="form-control" size="4" multiple="multiple">
                                                                <?php foreach ($jenis_komoditas as $value) :?>
                                                                    <option <?= !empty($_GET['komoditas_ids']) ? (in_array($value['id'], $_GET['komoditas_ids'])) ? 'selected' : null : null  ?> value="<?= $value['id'] ?>"><?= $value['nama']?></option>
                                                                    <?php foreach ($value['children'] as $child) :?>
                                                                        <option <?= !empty($_GET['komoditas_ids']) ? (in_array($child['id'], $_GET['komoditas_ids'])) ? 'selected' : null : null  ?> value="<?= $child['id'] ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $child['nama']?></option>
                                                                    <?php endforeach;?>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="formGroupExampleInput">Tipe Laporan</label>
                                                            <select id="filter_layout" name="tipe_laporan" class="form-control">
                                                                <option value="">Pilih laporan</option>
                                                                <option <?= !empty($_GET['tipe_laporan']) ? ($_GET['tipe_laporan'] == 'day') ? 'selected' : null : null  ?> value="day">Laporan Harian</option>
                                                                <option <?= !empty($_GET['tipe_laporan']) ? ($_GET['tipe_laporan'] == 'weekly') ? 'selected' : null : null  ?> value="weekly">Laporan Mingguan</option>
                                                                <option <?= !empty($_GET['tipe_laporan']) ? ($_GET['tipe_laporan'] == 'monthly') ? 'selected' : null : null  ?> value="monthly">Laporan Bulanan</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="formGroupExampleInput2">Lokasi Pasar</label>
                                                            <select id="filter_market_ids" name="pasar_ids[]" class="form-control" size="4" multiple="multiple">
                                                                <?php foreach ($pasar as $value) : ?>
                                                                    <option <?= !empty($_GET['pasar_ids']) ? (in_array($value->id, $_GET['pasar_ids'])) ? 'selected' : null : null  ?> value="<?= $value->id ?>"><?= $value->nama?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="formGroupExampleInput2">Tanggal</label>
                                                            <div class="clearfix"></div>
                                                            <div class="col-xs-6">
                                                                <input type="text" name="start_date" class="form-control" id="dt1" placeholder="Tanggal Mulai">
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <input type="text" name="end_date" class="form-control" id="dt2" placeholder="Tanggal Selesai">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <button type="submit" class="btn btn-primary btn-fill pull-left">Search</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <a href="<?php echo base_url('backend/komoditas/tambah_komoditas')?>"><button class="btn btn-info btn-fill pull-left">Tambah/Ubah Data Komoditas</button></a>
                                    <!--                                    <div class="text-right">-->
                                    <!--                                        <a href="--><?php //echo base_url('backend/komoditas/edit_komoditas')?><!--"><button type="button" class="table-prev btn btn-default"><i class="fa fa-pencil"></i>Edit Komoditas</button></a>-->
                                    <!--                                    </div>-->
                                    <div class="clearfix"></div>
                                    <br>
                                    <table id="report" class="table table-striped table-bordered table-condensed">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="45px"><?= $data['header']['penomoran'] ?></th>
                                            <th style="text-align: center; vertical-align: middle;"><?= $data['header']['nama'] ?></th>
                                            <?php foreach ($data['header']['tanggal'] as $key => $val) : ?>
                                                <th style="text-align: center; vertical-align: middle;"><?= $val?></th>
                                            <?php endforeach; ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($data['body'] as $key => $body) :?>
                                            <tr>
                                                <td class="text-center">
                                                    <span><?= $body['penomoran']?></span>
                                                </td>
                                                <td>
                                                    <div style="margin-left:<?= $body['em'] ?>em">
                                                        <span><?= $body['nama']?></span>
                                                    </div>
                                                </td>
                                                <?php foreach ($body['data'] as $key => $value) : ?>
                                                    <td class="text-right">
                                                        <span><?= $value?></span>
                                                    </td>
                                                <?php endforeach;?>
                                            </tr>
                                        <?php endforeach;?>
                                        </tbody>
                                    </table>
                                    <div class="text-center">
                                        <a href="<?php echo current_url().$data['url_prev']?>" class="table-prev btn btn-default"><i class="fa fa-chevron-left"></i> Kiri</a>
                                        <a href="<?php echo current_url().$data['url_next']?>" class="table-next btn btn-default">Kanan <i class="fa fa-chevron-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>