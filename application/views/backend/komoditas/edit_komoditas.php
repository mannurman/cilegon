<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="header">
						<h4 class="title">Tambah Komoditas</h4>
					</div>
					<div class="content">
						<div class="container-fluid">
							<div class="row">
					            <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="filter_start_date">Periode</label></br>
										<div class="input-group date" id="filter_start_date_container">
											<span class="input-group-addon btn btn-danger" onclick="jQuery(this).next().val(null)"><i class="fa fa-times"></i></span><input type="text" class="form-control" readonly="" style="background-color:white" value="11-04-2019" id="filter_start_date" name="filter_start_date"><span class="input-group-addon btn btn-info"><i class="fa fa-calendar"></i></span>
										</div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama Pasar</label></br>
										<select name="file_size" class="form-control">
											<option value="">Pasar Kranggot</option>
											<option value="">Pasar Kelapa</option>
										</select>													
                                    </div>
                                </div>
								<div class="col-md-12">
									<hr/>
									<table id="report" class="table table-striped table-bordered table-condensed">
									<thead>
									<tr>
										<th class="text-center" width="45px">No.</th>
										<th style="text-align: center; vertical-align: middle;">Komoditas</th>
										<th style="text-align: center; vertical-align: middle;">Harga (Rp)</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td class="text-center">
											<span><strong>I</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Beras</strong>
											</div>
										</td>
										<td style="margin-left:0em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Beras"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Beras Kualitas Bawah I (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Beras Kualitas Bawah I (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>2</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Beras Kualitas Bawah II (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Beras Kualitas Bawah II (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>3</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Beras Kualitas Medium I (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Beras Kualitas Medium I (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>4</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Beras Kualitas Medium II (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Beras Kualitas Medium II (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>5</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Beras Kualitas Super I (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Beras Kualitas Super I (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>6</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Beras Kualitas Super II (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Beras Kualitas Super II (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span><strong>II</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Daging Ayam</strong>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Daging Ayam"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Daging Ayam Ras Segar (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Daging Ayam Ras Segar (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span><strong>III</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Daging Sapi</strong>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Daging Sapi"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Daging Sapi Kualitas 1 (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Daging Sapi Kualitas 1 (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>2</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Daging Sapi Kualitas 2 (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Daging Sapi Kualitas 2 (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span><strong>IV</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Telur Ayam</strong>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Telur Ayam"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Telur Ayam Ras Segar (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Telur Ayam Ras Segar (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span><strong>V</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Bawang Merah</strong>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Bawang Merah"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Bawang Merah Ukuran Sedang (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Bawang Merah Ukuran Sedang (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span><strong>VI</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Bawang Putih</strong>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Bawang Putih"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Bawang Putih Ukuran Sedang (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Bawang Putih Ukuran Sedang (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span><strong>VII</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Cabai Merah</strong>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Cabai Merah"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Cabai Merah Besar (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Cabai Merah Besar (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>2</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Cabai Merah Keriting (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Cabai Merah Keriting (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span><strong>VIII</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Cabai Rawit</strong>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Cabai Rawit"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Cabai Rawit Hijau (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Cabai Rawit Hijau (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>2</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Cabai Rawit Merah (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Cabai Rawit Merah (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span><strong>IX</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Minyak Goreng</strong>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Minyak Goreng"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Minyak Goreng Curah (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Minyak Goreng Curah (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>2</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Minyak Goreng Kemasan Bermerk 1 (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Minyak Goreng Kemasan Bermerk 1 (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>3</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Minyak Goreng Kemasan Bermerk 2 (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Minyak Goreng Kemasan Bermerk 2 (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span><strong>X</strong></span>
										</td>
										<td>
											<div style="margin-left:0em">
												<strong>Gula Pasir</strong>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right">
												<strong><input type="text" name="" class="form-control" placeholder="Gula Pasir"></strong>
											</div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>1</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Gula Pasir Kualitas Premium (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Gula Pasir Kualitas Premium (kg)"></div>
										</td>
									</tr>
									<tr>
										<td class="text-center">
											<span>2</span>
										</td>
										<td>
											<div style="margin-left:1em">
												<span>Gula Pasir Lokal (kg)</span>
											</div>
										</td>
										<td style="margin-left:1em">
											<div class="text-right"><input type="text" name="" class="form-control" placeholder="Gula Pasir Lokal (kg)"></div>
										</td>
									</tr>
									</tbody>
									</table>
									<div class="text-center">
										<button type="submit" value="" class="btn btn-info btn-fill pull-left">Simpan Data</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>