<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="header">
						<h4 class="title">Tambah Komoditas</h4>
					</div>
					<div class="content">
                        <form name="<?php echo base_url('komoditas/tambah_komoditas')?>" enctype="multipart/form-data" action="" method="POST">
                            <div class="container-fluid">
                                <?php if(!empty(validation_errors())) : ?>
                                    <div class="alert alert-danger" role="alert">
                                        <?= validation_errors() ?>
                                    </div>
                                <?php endif; ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="filter_start_date">Periode</label></br>
                                            <div class="input-group date" id="filter_start_date_container">
                                                <span class="input-group-addon btn btn-danger" onclick="jQuery(this).next().val(null)"><i class="fa fa-times"></i></span>
                                                <input type="text" readonly class="form-control datepicker" style="background-color:white" id="filter_start_date" name="tanggal">
                                                <span class="input-group-addon btn btn-info"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Nama Pasar</label></br>
                                            <select name="pasar_id" class="form-control" id="pasar-data">
                                                <option value="">Pilih Pasar</option>
                                                <?php foreach ($pasar as $value) : ?>
                                                <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <hr/>
                                        <table id="report" class="table table-striped table-bordered table-condensed">
                                            <thead>
                                            <tr>
                                                <th class="text-center" width="45px">No.</th>
                                                <th style="text-align: center; vertical-align: middle;">Komoditas</th>
                                                <th style="text-align: center; vertical-align: middle;">Harga (Rp)</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($jenis_komoditas as $val) : ?>
                                                    <tr>
                                                        <td class="text-center">
                                                            <?php if (is_null($val->parent_id)): ?>
                                                            <span><strong><?= $val->penomoran ?></strong></span>
                                                            <?php else: ?>
                                                                <span><?= $val->penomoran ?></span>
                                                            <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <div style="margin-left:0em">
                                                                <?php if (is_null($val->parent_id)): ?>
                                                                    <span><strong><?= $val->nama ?></strong></span>
                                                                <?php else: ?>
                                                                    <span><?= $val->nama ?></span>
                                                                <?php endif; ?>
                                                            </div>
                                                        </td>
                                                        <td style="margin-left:0em">
                                                            <div class="text-right">
                                                                <strong><input type="text" id="komoditas-<?= $val->id ?>" name="harga[<?= $val->id ?>]" class="form-control" placeholder="<?= $val->nama ?>"></strong>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach;?>
                                            </tbody>
                                        </table>
                                        <div class="text-center">
                                            <button type="submit" value="" class="btn btn-info btn-fill pull-left">Simpan Data</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>