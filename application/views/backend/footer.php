        <footer class="footer">
            <div class="container-fluid">
				<p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> <a href="https://cilegon.go.id">eGov Pemerintah Kota Cilegon</a>. All Right Reserved.
                </p>
            </div>
        </footer>
    </div>
</div>
</body>
    <script src="<?php echo base_url('/assets/js/jquery-1.10.2.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('/assets/js/jquery-ui.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('/assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('/assets/js/bootstrap-checkbox-radio-switch.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/bootstrap-notify.js'); ?>"></script>
	<script src="<?php echo base_url('/assets/js/light-bootstrap-dashboard.js'); ?>"></script>
	<script src="<?php echo base_url('/assets/js/demo.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/js/chartist.min.js'); ?>"></script>
    <script src="<?php echo base_url('/assets/redactor/redactor.min.js'); ?>"></script>

    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                dateFormat: 'dd-mm-yy'
            });

            $("#dt1").datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function () {
                    var dt2 = $('#dt2');
                    var startDate = $(this).datepicker('getDate');
                    //add 30 days to selected date
                    startDate.setDate(startDate.getDate() + 30);
                    var minDate = $(this).datepicker('getDate');
                    var dt2Date = dt2.datepicker('getDate');
                    //difference in days. 86400 seconds in day, 1000 ms in second
                    var dateDiff = (dt2Date - minDate)/(86400 * 1000);

                    //dt2 not set or dt1 date is greater than dt2 date
                    if (dt2Date == null || dateDiff < 0) {
                        dt2.datepicker('setDate', minDate);
                    }
                    //dt1 date is 30 days under dt2 date
                    else if (dateDiff > 30){
                        dt2.datepicker('setDate', startDate);
                    }
                    //sets dt2 maxDate to the last day of 30 days window
                    dt2.datepicker('option', 'maxDate', startDate);
                    //first day which can be selected in dt2 is selected date in dt1
                    dt2.datepicker('option', 'minDate', minDate);
                }
            });
            $('#dt2').datepicker({
                dateFormat: "dd-mm-yy",
            });
        });
    </script>
	<script type="text/javascript">
    	$(document).ready(function(){
        	// demo.initChartist();
        	$.notify({
            	icon: 'pe-7s-user',
            	message: "Selamat Datang di <b>Dashboard e-Harga Pangan</b> </br>"
            },{
                type: 'info',
                timer: 2000
            });
    	});
        $('#filter_start_date, #pasar-data').on('change', function () {

            var date_filter = $('#filter_start_date').val();
            var pasar = $('#pasar-data').val();

            if(date_filter && pasar) {
                $.ajax({
                    url: "<?= base_url('backend/komoditas/get_komoditas')?>",
                    type : 'post',
                    data : {tanggal : date_filter, pasar_id : pasar},
                    dataType: "json",
                    success: function(result) {
                        $.each(result.data, function( key, value ) {
                            $('#komoditas-'+value.jenis_komoditas_id).val(value.harga);
                        });
                    }
                });
            }
        });
        $R('.text-redactor', {
            focus: true,
            minHeight: '200px'
        });
	</script>
</html>
