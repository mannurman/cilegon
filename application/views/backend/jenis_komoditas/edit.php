<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Data Jenis Komoditas</h4>
                    </div>
                    <div class="content">
                        <form name="<?php echo base_url('backend/jenis_komoditas/add')?>" enctype="multipart/form-data" action="" method="POST">
                            <?php $this->load->view("backend/jenis_komoditas/_form") ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>