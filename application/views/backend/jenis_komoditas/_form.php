<div class="clearfix"></div>
<div class="col-md-12">
    <div class="form-group <?php echo form_error('parent_id') ? 'has-error':'' ?>">
        <label>Nama</label></br>
        <select name="parent_id" class="form-control">
            <?php foreach ($parent as $value) : ?>
                <option <?= !empty($model->parent_id) ? ($model->parent_id == $value->id) ? 'selected' : null : null  ?> value="<?= $value->id ?>"><?= $value->nama ?></option>
            <?php endforeach; ?>
        </select>
        <div class="alert-error"><?php echo form_error('parent_id') ?></div>
    </div>
</div>
<div class="col-md-12">
    <div class="form-group <?php echo form_error('nama') ? 'has-error':'' ?>">
        <label>Nama</label></br>
        <input type="text" name="nama" value="<?= $model->nama ?? null ?>" class="form-control" placeholder="Nama Pasar">
        <div class="alert-error"><?php echo form_error('nama') ?></div>
    </div>
</div>

<button type="submit" value="" class="btn btn-info btn-fill pull-left">Simpan Data</button>
<div class="clearfix"></div>