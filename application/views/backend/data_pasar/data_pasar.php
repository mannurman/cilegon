<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Data Pasar</h4>
                    </div>
                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Filtering</div>
                                        <div class="panel-body">
                                            <form action="" method="GET">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="nama" value="<?= $_GET['nama'] ?? null ?>" class="form-control" placeholder="Nama Pasar">
                                                        </div>
                                                        <div class="form-group">
                                                            <select name="jenis_pasar_id" class="form-control">
                                                                <option value="">Pilih Jenis Pasar</option>
                                                                <?php foreach ($jenis_pasar as $value) : ?>
                                                                    <option value="<?= $value->id ?>"><?= $value->nama ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="text" name="alamat_pasar" value="<?= $_GET['alamat_pasar'] ?? null ?>" class="form-control" placeholder="Alamat Pasar">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pull-right">
                                                    <button type="submit" class="btn btn-primary btn-fill pull-left">Search</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <a href="<?php echo base_url('backend/pasar/add')?>"><button class="btn btn-info btn-fill pull-left">Tambah Data</button></a>
                                    <div class="content table-responsive table-full-width">
                                        <table class="table table-hover table-striped">
                                        <thead>
                                            <th>Nama Pasar</th>
                                            <th>Jenis Pasar</th>
                                            <th>Alamat Pasar</th>
                                            <th>Aksi</th>
                                        </thead>
                                            <?php foreach ($pasar as $value) : ?>
                                        <tr>
                                            <td><?= $value->nama ?></td>
                                            <td><?= $value->jenis_pasar_nama ?></td>
                                            <td><?= $value->alamat_pasar ?></td>
                                            <td>
                                                <a href="<?php echo base_url('backend/pasar/edit/'. $value->id) ?>"><div class="fa fa-pencil"></div></a>
                                                <a href="<?= base_url('backend/pasar/delete/'. $value->id) ?>" onclick="return confirm('apa anda yakin?')"><div class="fa fa-trash"></div></a>
                                            </td>
                                        </tr>
                                            <?php endforeach; ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>