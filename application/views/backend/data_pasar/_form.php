<div class="row">
    <div class="col-md-6">
        <div class="form-group <?php echo form_error('jenis_pasar') ? 'has-error':'' ?>">
            <label>Jenis Pasar</label></br>
            <select name="jenis_pasar_id" class="form-control">
                <?php foreach ($jenis_pasar as $value) : ?>
                <option <?= !empty($pasar->jenis_pasar_id) ? ($pasar->jenis_pasar_id == $value->id) ? 'selected' : null : null  ?> value="<?= $value->id ?>"><?= $value->nama ?></option>
                <?php endforeach; ?>
            </select>
            <div class="alert-error"><?php echo form_error('jenis_pasar') ?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group <?php echo form_error('nama') ? 'has-error':'' ?>">
            <label>Nama Pasar</label></br>
            <input type="text" name="nama" value="<?= $pasar->nama ?? null?>" class="form-control" placeholder="Nama Pasar">
            <div class="alert-error"><?php echo form_error('nama') ?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Lokasi Pasar</label>
            <object data="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d93763.03776711164!2d106.04071814999612!3d-5.981314573262656!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sid!2sid!4v1556070692989!5m2!1sid!2sid" width="100%" height="500" frameborder="0" style="border:0"></object>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Titik Koordinat</label>
            <input type="text" name="latitude" class="form-control" placeholder="Latitude"></p>
            <input type="text" name="longitude" class="form-control" placeholder="Longitude">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Alamat Pasar</label>
            <input type="text" name="alamat_pasar" value="<?= $pasar->nama ?? null?>" class="form-control" placeholder="Alamat Pasar">
        </div>
    </div>
</div>
<button type="submit" value="" class="btn btn-info btn-fill pull-left">Simpan Data</button>
<div class="clearfix"></div>