<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container t3-sl t3-sl-1">
    <div class="t3-spotlight t3-spotlight-1 row">
        <div class="col-xs-12">
            <div class="t3-module module " id="Mod95">
                <div class="module-inner">
                    <div class="module-ct">
                        <div id="mod_gtpihps_map-95" class="mod_gtpihps_map">
                            <div id="t3-content" class="t3-content col-xs-12">
                                <div id="system-message-container"></div>
                                <div id="com_gtpihps" class="item-page">
                                    <div class="page-header">
                                        <h1>Informasi Harga <?= $jenis_pasar->nama ?? null?> Per <?= ucfirst($jenis_harga) ?></h1>
                                    </div>
                                    <div class="row">
                                        <?php $this->load->view("frontend/informasi_harga/search") ?>
                                        <div class="col-md-9">
                                            <div id="report-header">
                                                <h4>Perkembangan Harga Pangan</h4>
                                                <div>
                                                    <span>Periode</span> : <?= $data['detail']['period']?>
                                                </div>
                                                <div>
                                                    <span>Pasar</span> : <?= $data['detail']['pasar']?>
                                                </div>
                                                <div>
                                                    <span>Tipe Laporan</span> : <?= $data['detail']['tipe_laporan']?>
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="text-center">
                                                <a href="<?php echo current_url().$data['url_prev']?>" class="table-prev btn btn-default"><i class="fa fa-chevron-left"></i> Kiri</a>
                                                <a href="<?php echo current_url().$data['url_next']?>" class="table-next btn btn-default">Kanan <i class="fa fa-chevron-right"></i></a>
                                            </div>
                                            <br/>
                                            <table id="report" class="table table-striped table-bordered table-condensed">
                                                <thead>
                                                <tr>
                                                    <th class="text-center" width="45px"><?= $data['header']['penomoran'] ?></th>
                                                    <th style="text-align: center; vertical-align: middle;"><?= $data['header']['nama'] ?></th>
                                                    <?php foreach ($data['header']['tanggal'] as $key => $val) : ?>
                                                        <th style="text-align: center; vertical-align: middle;"><?= $val?></th>
                                                    <?php endforeach; ?>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($data['body'] as $key => $body) :?>
                                                <tr>
                                                    <td class="text-center">
                                                        <span><?= $body['penomoran']?></span>
                                                    </td>
                                                    <td>
                                                        <div style="margin-left:<?= $body['em'] ?>em">
                                                            <span><?= $body['nama']?></span>
                                                        </div>
                                                    </td>
                                                        <?php foreach ($body['data'] as $key => $value) : ?>
                                                            <td class="text-right">
                                                                <span><?= $value?></span>
                                                            </td>
                                                        <?php endforeach;?>
                                                    <?php endforeach;?>
                                                </tbody>
                                            </table>
                                            <div class="text-center">
                                                <a href="<?php echo current_url().$data['url_prev']?>" class="table-prev btn btn-default"><i class="fa fa-chevron-left"></i> Kiri</a>
                                                <a href="<?php echo current_url().$data['url_next']?>" class="table-next btn btn-default">Kanan <i class="fa fa-chevron-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SPOTLIGHT -->
</div>
