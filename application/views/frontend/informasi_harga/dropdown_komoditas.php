<div class="form-group">
    <label for="filter_commodity_ids">Komoditas</label>
    <select id="filter_commodity_ids" name="komoditas_ids[]" class="form-control" size="8" multiple="multiple">
        <?php foreach ($jenis_komoditas as $value) :?>
            <option <?= !empty($_GET['komoditas_ids']) ? (in_array($value['id'], $_GET['komoditas_ids'])) ? 'selected' : null : null  ?> value="<?= $value['id'] ?>"><?= $value['nama']?></option>
            <?php foreach ($value['children'] as $child) :?>
                <option <?= !empty($_GET['komoditas_ids']) ? (in_array($child['id'], $_GET['komoditas_ids'])) ? 'selected' : null : null  ?> value="<?= $child['id'] ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $child['nama']?></option>
            <?php endforeach;?>
        <?php endforeach;?>
    </select>
</div>