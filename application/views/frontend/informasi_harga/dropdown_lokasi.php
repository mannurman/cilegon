<div class="form-group">
    <label for="filter_market_ids">Lokasi Pasar</label>
    <select id="filter_market_ids" name="pasar_ids[]" class="form-control" size="3" multiple="multiple">
        <?php foreach ($pasar as $value) : ?>
            <option <?= !empty($_GET['pasar_ids']) ? (in_array($value->id, $_GET['pasar_ids'])) ? 'selected' : null : null  ?> value="<?= $value->id ?>"><?= $value->nama?></option>
        <?php endforeach;?>
    </select>
</div>