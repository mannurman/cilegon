<div class="col-md-3">
    <form action="" method="GET" name="adminForm" class="form-filter" role="form">
        <?php if ($jenis_harga == 'komoditas') : ?>
            <?php $this->load->view("frontend/informasi_harga/dropdown_komoditas") ?>
        <?php else: ?>
            <?php $this->load->view("frontend/informasi_harga/dropdown_lokasi") ?>
        <?php endif;?>

        <div class="form-group">
            <label for="filter_regency_ids">Tipe Laporan</label>
            <select id="filter_layout" name="tipe_laporan" class="form-control">
                <option value="">Pilih laporan</option>
                <option <?= !empty($_GET['tipe_laporan']) ? ($_GET['tipe_laporan'] == 'day') ? 'selected' : null : null  ?> value="day">Laporan Harian</option>
                <option <?= !empty($_GET['tipe_laporan']) ? ($_GET['tipe_laporan'] == 'weekly') ? 'selected' : null : null  ?> value="weekly">Laporan Mingguan</option>
                <option <?= !empty($_GET['tipe_laporan']) ? ($_GET['tipe_laporan'] == 'monthly') ? 'selected' : null : null  ?> value="monthly">Laporan Bulanan</option>
            </select>
        </div>
        <div class="form-group">
            <label for="filter_start_date">Tanggal Mulai</label>
            <div class="input-group date" id="filter_start_date_container">
                <span class="input-group-addon btn btn-danger" onclick="jQuery(this).next().val(null)"><i class="fa fa-times"></i></span>
                <input type="text" class="form-control" readonly style="background-color:white" id="dt1" value="<?= $_GET['start_date'] ?? null ?>" name="start_date">
                <span class="input-group-addon btn btn-info"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
        <div class="form-group">
            <label for="filter_end_date">Tanggal Selesai</label>
            <div class="input-group date" id="filter_end_date_container">
                <span class="input-group-addon btn btn-danger" onclick="jQuery(this).next().val(null)"><i class="fa fa-times"></i></span>
                <input type="text" class="form-control" readonly style="background-color:white" id="dt2" value="<?= $_GET['end_date'] ?? null ?>" name="end_date">
                <span class="input-group-addon btn btn-info"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
        <input type="hidden" name="page" value="<?= $_GET['page'] ?? 0 ?>">
        <button id="btnReport" type="submit" name="submit" value="search" class="btn btn-primary btn-lg btn-block" onclick="jQuery('#format').val('html')">
            <i class="fa fa-file-text"></i> Lihat Laporan </button>
        <button id="btnDownload" type="submit" name="submit" value="excel"  class="btn btn-success btn-lg btn-block" onclick="jQuery('#format').val('xls')">
            <i class="fa fa-download"></i> Download </button>
    </form>
</div>