<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container t3-sl t3-sl-1">
    <div class="t3-spotlight t3-spotlight-1 row">
        <div class="col-xs-12">
            <div class="t3-module module " id="Mod95">
                <div class="module-inner">
                    <div class="module-ct">
                        <div id="mod_gtpihps_map-95" class="mod_gtpihps_map">
                            <div id="t3-content" class="t3-content col-xs-12">
                                <div id="system-message-container"></div>
                                <div id="com_gtpihps" class="item-page">
                                    <div class="page-header">
                                        <h1>Tutorial</h1>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?= $description ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
