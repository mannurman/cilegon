<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container t3-sl t3-sl-1">
    <div class="t3-spotlight t3-spotlight-1 row">
        <div class="col-xs-12">
            <div class="t3-module module " id="Mod95">
                <div class="module-inner">
                    <div class="module-ct">
                        <div id="mod_gtpihps_map-95" class="mod_gtpihps_map">
                            <form class="form-vertical" role="form" id="mapForm" name="mapForm" method="GET" action="">
                                <div class="row-fluid">
                                    <div class="control-group col-md-3">
                                        <label class="hasTip" for="commodity_id">Komoditas</label>
                                        <div class="controls">
                                            <select id="commodity_id" name="komoditas_id" class="form-control">
                                                <option value="">Pilih komoditas</option>
                                                <?php foreach ($jenis_komoditas as $value) :?>
                                                    <option <?= !empty($_GET['komoditas_id']) ? ($_GET['komoditas_id'] == $value['id']) ? 'selected' : null : null  ?> value="<?= $value['id'] ?>"><?= $value['nama']?></option>
                                                    <?php foreach ($value['children'] as $child) :?>
                                                        <option <?= !empty($_GET['komoditas_id']) ? ($_GET['komoditas_id'] == $child['id']) ? 'selected' : null : null  ?> value="<?= $child['id'] ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $child['nama']?></option>
                                                    <?php endforeach;?>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group col-md-3">
                                        <label class="hasTip" for="jenis_pasar_id">Jenis Pasar</label>
                                        <div class="controls">
                                            <select id="price_type_id" name="jenis_pasar_id" class="form-control">
                                                <option value="">Pilih jenis pasar</option>
                                                <?php foreach ($jenis_pasar as $value) : ?>
                                                    <option <?= !empty($_GET['jenis_pasar_id']) ? ($_GET['jenis_pasar_id'] == $value->id) ? 'selected' : null : null  ?> value="<?= $value->id ?>"><?= $value->nama?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group col-md-3">
                                        <label class="hasTip" for="pasar_id">Lokasi Pasar</label>
                                        <div class="controls">
                                            <select id="data_type" name="pasar_id" class="form-control">
                                                <option value="">Pilih pasar</option>
                                                <?php foreach ($pasar as $value) : ?>
                                                    <option <?= !empty($_GET['pasar_id']) ? ($_GET['pasar_id'] == $value->id) ? 'selected' : null : null  ?> value="<?= $value->id ?>"><?= $value->nama?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group col-md-2">
                                        <label class="hasTip" for="date">Tanggal</label>
                                        <div class="controls">
                                            <div class="input-group date" id="date_container">
                                                <span class="input-group-addon btn btn-danger" onclick="jQuery(this).next().val(null)"><i class="fa fa-times"></i></span>
                                                <input type="text" class="form-control datepicker" value="<?= $_GET['tanggal'] ?? null ?>" readonly="" style="background-color:white" id="date" name="tanggal">
                                                <span class="input-group-addon btn btn-info"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="control-group col-md-2">-->
<!--                                        <label class="hasTip" for="layout">Periode Perbandingan</label>-->
<!--                                        <div class="controls">-->
<!--                                            <select id="layout" name="tipe_laporan" class="form-control">-->
<!--                                                <option value="day">Harian</option>-->
<!--                                                <option value="weekly">Mingguan</option>-->
<!--                                                <option value="monthly">Bulanan</option>-->
<!--                                            </select>-->
<!--                                        </div>-->
<!--                                    </div>-->
                                    <div class="control-group col-md-1">
                                        <button class="btn btn-primary btn-lg btn-block" type="submit"><i class="fa fa-fw fa-arrow-down"></i></button>
                                    </div>
                                </div>
                            </form>
                            <div class="row data-display">
                                <div class="col-md-12">
                                    </br><center><img src="<?php echo base_url('assets/img/pemkot.png'); ?>" width="500px" height="100%"/></center></br>
                                </div>
                                <div class="col-md-9">
                                    <div id="chartContainer" style="height: 370px; max-width: auto; margin: 0px auto;"></div>
                                </div>
                                <div class="col-md-3">
                            <table id="report" class="table table-striped table-bordered table-condensed">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="45px">No.</th>
                                        <th style="text-align: center; vertical-align: middle;">Komoditas</th>
                                        <th style="text-align: center; vertical-align: middle;">Harga Rerata (Rp)</th>
                                    </tr>
                                </thead>
                            <tbody>
                            <?php foreach ($parent_komoditas as $value) : ?>
                                <tr>
                                    <td class="text-center">
                                        <span><strong><?= $value['nomor'] ?></strong></span>
                                    </td>
                                    <td>
                                        <div style="margin-left:0em">
                                            <strong><?= $value['nama'] ?></strong>
                                        </div>
                                    </td>
                                    <td style="margin-left:0em">
                                        <div class="text-right">
                                            <strong><?= $value['harga'] ?></strong>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tbody>
                            </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer", {
            title:{
                text: "Grafik Rata-Rata Perkembangan Harga Pangan"
            },
            axisY: {
                title: "Harga Pangan (Rp)",
                lineColor: "#4F81BC",
                tickColor: "#4F81BC",
                labelFontColor: "#4F81BC",
                gridThickness: 0
            },
            axisY2: {
                title: "Persentase",
                suffix: "%",
                gridThickness: 0,
                lineColor: "#C0504E",
                tickColor: "#C0504E",
                labelFontColor: "#C0504E"
            },
            data: [{
                type: "column",
                dataPoints: <?= $data_chart ?>
            }]
        });
        chart.render();
        createPareto();

        function createPareto(){
            var dps = [];
            var yValue, yTotal = 0, yPercent = 0;

            for(var i = 0; i < chart.data[0].dataPoints.length; i++)
                yTotal += chart.data[0].dataPoints[i].y;

            for(var i = 0; i < chart.data[0].dataPoints.length; i++) {
                yValue = chart.data[0].dataPoints[i].y;
                yPercent += (yValue / yTotal * 100);
                dps.push({label: chart.data[0].dataPoints[i].label, y: yPercent });
            }
            chart.addTo("data", {type:"line", axisYType: "secondary", yValueFormatString: "0.##'%'", indexLabel: "{y}", indexLabelFontColor: "#C24642", dataPoints: dps});
            chart.axisY[0].set("maximum", yTotal, false);
            chart.axisY2[0].set("maximum", 105, false );
            chart.axisY2[0].set("interval", 25 );
        }

    }
</script>
<script src="<?php echo base_url('assets/js/canvasjs.min.js'); ?>"></script>
