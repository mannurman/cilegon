<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="id-id">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>e-Harga Pangan - Beranda</title>
	<link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico')?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css')?>"/>
    <link href="<?php echo base_url('/assets/css/jquery-ui.css'); ?>" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/template.css')?>"/>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<style type="text/css">
	.container {width: 1124px !important;} .t3-wrapper, .wrap {min-width: 1124px !important;}
	/* css menu dropdown toggle */
	.mega-dropdown-inner {padding: 12px;}
	.dropdown-menu .mega-inner ul {  padding: 0;}
	.dropdown-menu .mega-inner ul li {padding: 5px;}
	.dropdown-menu .mega-inner ul li a {color: #555; padding: 5px; }
	/* css menu dropdown  */
	.dropdown{position:relative;display:inline-block}
    .dropdown-contentx{display:none;position:absolute;min-width:100%;box-shadow:0 8px 16px 0 rgba(0,0,0,0.2);z-index:1}
    .dropdown-contentx a{color:#000;padding:12px 16px;text-decoration:none;display:block}
    .dropdown-contentx a:hover{background-color:#ffffff}
    .dropdown:hover .dropdown-contentx{display:block; z-index: 100;}
	</style>
</head>

<body class="home beranda">
	<div class="t3-wrapper">
		<nav id="t3-mainnav" class="wrap navbar navbar-default t3-mainnav">
			<div class="container">
				<div class="row">
					<div class="logo pull-left">
						<div class="logo-image">
							<a href="<?php echo base_url('')?>" title="e-Harga Pangan">
								<img class="logo-img" src="<?php echo base_url('assets/img/logo.png')?>" alt="e-Harga Pangan"/>
							</a>
							<small class="site-slogan">Pusat Informasi Harga Pangan Kota Cilegon</small>
						</div>
					</div>
					<div class="navbar-header pull-left"></div>
					<div class="t3-navbar-collapse navbar-collapse collapse"></div>
					<div class="t3-navbar navbar-collapse collapse">
						<div class="t3-megamenu">
							<ul class="nav navbar-nav level0">
<!--								<li class="current active">-->
								<li class="current">
									<a href="<?php echo base_url('')?>">Beranda</a>
								</li>
								<li class="dropdown" data-level="1">
									<a class="dropbtnx" href="#" data-toggle="dropdown">Informasi Harga <em class="caret"></em></a>
									<div class="dropdown-contentx nav-child dropdown-menu mega-dropdown-menu">
										<div class="mega-dropdown-inner">
											<div class="row">
												<div class="col-xs-12 mega-col-nav">
													<div class="mega-inner">
														<ul class="mega-nav level1">
                                                            <?php foreach ($jenis_pasar as $value) : ?>
															<li class="dropdown-submenu mega">
																<a class="dropdown-toggle" href="#"><?= $value->nama?></a>
																<div class="nav-child dropdown-menu mega-dropdown-menu">
																	<div class="mega-dropdown-inner">
																		<div class="row">
																			<div class="col-xs-12 mega-col-nav">
																				<div class="mega-inner">
																					<ul class="mega-nav level2">
																						<li>
																							<a href="<?php echo site_url('pangan/informasi_harga/lokasi/' . $value->id) ?>">Per Lokasi</a>
																						</li>
																						<li>
																							<a href="<?php echo site_url('pangan/informasi_harga/komoditas/' . $value->id) ?>">Per Komoditas</a>
																						</li>
																					</ul>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</li>
                                                            <?php endforeach; ?>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li class="dropdown mega">
									<a class="dropbtnx" href="#">Informasi Aplikasi <em class="caret"></em></a>
									<div class="dropdown-contentx nav-child dropdown-menu mega-dropdown-menu">
										<div class="mega-dropdown-inner">
											<div class="row">
												<div class="col-xs-12 mega-col-nav">
													<div class="mega-inner">
														<ul class="mega-nav level1">
															<li>
																<a href="<?php echo site_url('pangan/faq') ?>">Syarat dan Ketentuan</a>
															</li>
															<li>
																<a href="<?php echo site_url('pangan/petunjuk') ?>">Petunjuk Aplikasi</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
								<li class="dropdown mega">
									<a class="dropbtnx" href="#"><img src="<?php echo base_url('assets/img/user.png')?>" alt="User"/><em class="caret"></em></a>
									<div class="dropdown-contentx nav-child dropdown-menu mega-dropdown-menu" style="width: 250px" data-width="250">
										<div class="mega-dropdown-inner">
											<div class="row">
												<div class="col-xs-12 mega-col-module">
													<div class="mega-inner">
														<div class="t3-module module " id="Mod92">
															<div class="module-inner">
																<div class="module-ct">
																	<form action="<?= base_url('login/login')?>" method="post" id="login-form">
																		<div id="form-login-username" class="form-group">
																			<div class="input-group">
																				<span class="input-group-addon"><i class="fa fa-user fa-fw" title="Nama Pengguna"></i></span>
																				<input id="modlgn-username" type="text" name="username" class="input required" tabindex="0" size="18" placeholder="Nama Pengguna" required aria-required="true"/>
																			</div>
																		</div>
																		<div id="form-login-password" class="form-group">
																			<div class="input-group">
																				<span class="input-group-addon"><i class="fa fa-lock fa-fw" title="Sandi"></i></span>
																				<input id="modlgn-passwd" type="password" name="password" class="input required" tabindex="0" size="18" placeholder="Kata Sandi" required aria-required="true"/>
																			</div>
																		</div>
																		<div id="form-login-remember" class="form-group">
																			<label for="modlgn-remember" class="checkbox"><input id="modlgn-remember" type="checkbox" name="remember" class="input" value="Ingatkan Saya"/>Ingatkan Saya</label>
																		</div>
																		<div class="form-group">
																			<input type="button" name="Submit" id="login-button" class="btn btn-primary btn-lg btn-block" value="Login"/>
																		</div>
																	</form>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</nav>