<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <footer id="t3-footer" class="wrap t3-footer">
        <section class="t3-copyright">
            <div class="container">
                <div class="copyright ">
                    <div class="module">
                        <small>Page rendered in <strong>{elapsed_time}</strong> seconds. &copy; 2019 eGov Pemerintah Kota Cilegon - Pusat Informasi Harga Pangan Pemerintah Kota Cilegon</small>
                    </div>
                </div>
            </div>
        </section>
    </footer>
	</div>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="<?php echo base_url('/assets/js/jquery-ui.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('/assets/js/bootstrap-notify.js'); ?>"></script>
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                dateFormat: 'dd-mm-yy'
            });

            $("#dt1").datepicker({
                dateFormat: "dd-mm-yy",
                onSelect: function () {
                    var dt2 = $('#dt2');
                    var startDate = $(this).datepicker('getDate');
                    //add 30 days to selected date
                    startDate.setDate(startDate.getDate() + 30);
                    var minDate = $(this).datepicker('getDate');
                    var dt2Date = dt2.datepicker('getDate');
                    //difference in days. 86400 seconds in day, 1000 ms in second
                    var dateDiff = (dt2Date - minDate)/(86400 * 1000);

                    //dt2 not set or dt1 date is greater than dt2 date
                    if (dt2Date == null || dateDiff < 0) {
                        dt2.datepicker('setDate', minDate);
                    }
                    //dt1 date is 30 days under dt2 date
                    else if (dateDiff > 30){
                        dt2.datepicker('setDate', startDate);
                    }
                    //sets dt2 maxDate to the last day of 30 days window
                    dt2.datepicker('option', 'maxDate', startDate);
                    //first day which can be selected in dt2 is selected date in dt1
                    dt2.datepicker('option', 'minDate', minDate);
                }
            });
            $('#dt2').datepicker({
                dateFormat: "dd-mm-yy",
            });
        });
    </script>
	<script type="text/javascript">
		$(function() {
            $('.dropdown-toggle').click(function(){
                $(this).next('.dropdown-menu').toggle();
            });

            $(document).click(function(e) {
                var target = e.target;
                if (!$(target).is('.dropdown-toggle') && !$(target).parents().is('.dropdown-toggle')) {
                  $('.dropdown-menu').next();
                }
            });
		});

		$('#login-button').on('click', function () {
		    var data = {
		        username : $('#modlgn-username').val(),
                password : $('#modlgn-passwd').val(),
            };

            $.ajax({
                url: "<?= base_url('login/login')?>",
                type : 'post',
                data : data,
                dataType: "json",
                success: function(result) {
                    if (!result.status) {
                        $.notify({
                            icon: 'pe-7s-user',
                            message: "Login gagal silahkan check kembali <b>username dan password </b> anda</br>"
                        },{
                            type: 'info',
                            timer: 1000
                        });
                    } else {
                        window.location.href = "/admin";

                    }
                }
            });
        });

	</script> 
</body>
</html>