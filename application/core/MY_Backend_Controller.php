<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Backend_Controller extends CI_Controller
{
    protected $data = array();
    function __construct()
    {
        parent::__construct();
        $this->load->model('jenis_pasar_model');
        $this->load->library('session');
        $this->load->helper('new_helper');

        if($this->session->userdata('status') != "login"){
            redirect(base_url("/"));
        }
    }

    public function load_view_backend($view, $data = [])
    {
        $data = array_merge($data, ['role' => $this->session->userdata('role')]);

        $this->load->view('backend/header', $data);
        $this->load->view($view, $data);
        $this->load->view('backend/footer');
    }
}