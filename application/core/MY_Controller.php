<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $data = array();
    function __construct()
    {
        parent::__construct();
        $this->load->model('jenis_pasar_model');
    }

    public function load_view_frontend($view, $data = [])
    {
        $jenis_pasar = $this->jenis_pasar_model->getAll();

        $this->load->view('frontend/layouts/header', compact('jenis_pasar'));
        $this->load->view($view, $data);
        $this->load->view('frontend/layouts/footer');
    }
}