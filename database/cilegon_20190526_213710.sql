-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE TABLE "jenis_komoditas" ------------------------------
CREATE TABLE `jenis_komoditas` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`parent_id` Int( 11 ) NULL,
	`penomoran` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`nama` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 33;
-- -------------------------------------------------------------


-- CREATE TABLE "jenis_pasar" ----------------------------------
CREATE TABLE `jenis_pasar` ( 
	`id` BigInt( 20 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`nama` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------


-- CREATE TABLE "komoditas" ------------------------------------
CREATE TABLE `komoditas` ( 
	`id` BigInt( 20 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`tanggal` Date NOT NULL,
	`pasar_id` Int( 11 ) NULL,
	`jenis_komoditas_id` Int( 11 ) NULL,
	`harga` Decimal( 8, 2 ) NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 219;
-- -------------------------------------------------------------


-- CREATE TABLE "pasar" ----------------------------------------
CREATE TABLE `pasar` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`nama` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`jenis_pasar` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`latitude` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`longitude` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`alamat_pasar` Text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	`jenis_pasar_id` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- CREATE TABLE "users" ----------------------------------------
CREATE TABLE `users` ( 
	`id` Int( 10 ) UNSIGNED AUTO_INCREMENT NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`username` VarChar( 100 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`password` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
	`created_at` Timestamp NULL,
	`updated_at` Timestamp NULL,
	`level` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `users_username_unique` UNIQUE( `username` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "settings" -------------------------------------
CREATE TABLE `settings` ( 
	`key` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`value` LongText CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- Dump data of "jenis_komoditas" --------------------------
INSERT INTO `jenis_komoditas`(`id`,`parent_id`,`penomoran`,`nama`,`created_at`,`updated_at`) VALUES 
( '1', NULL, 'I', 'Beras', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '2', '1', '1', 'Beras Kualitas Bawah I (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '3', '1', '2', 'Beras Kualitas Bawah II (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '4', '1', '3', 'Beras Kualitas Medium I (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '5', '1', '4', 'Beras Kualitas Medium II (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '6', '1', '5', 'Beras Kualitas Super I (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '7', '1', '6', 'Beras Kualitas Super II (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '8', NULL, 'II', 'Daging Ayam', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '9', '8', '1', 'Daging Ayam Ras Segar (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '10', NULL, 'III', 'Daging Sapi', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '11', '10', '1', 'Daging Sapi Kualitas 1 (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '12', '10', '2', 'Daging Sapi Kualitas 2 (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '13', NULL, 'IV', 'Telur Ayam', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '14', '13', '1', 'Telur Ayam Ras Segar (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '15', NULL, 'V', 'Bawang Merah', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '16', '15', '1', 'Bawang Merah Ukuran Sedang (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '18', '17', '1', 'Bawang Putih Ukuran Sedang (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '19', NULL, 'VII', 'Cabai Merah', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '20', '19', '1', 'Cabai Merah Besar (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '21', '19', '2', 'Cabai Merah Keriting (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '22', NULL, 'VIII', 'Cabai Rawit', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '23', '22', '1', 'Cabai Rawit Hijau (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '24', '22', '2', 'Cabai Rawit Merah (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '25', NULL, 'IX', 'Minyak Goreng', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '26', '25', '1', 'Minyak Goreng Curah (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '27', '25', '2', 'Minyak Goreng Kemasan Bermerk 1 (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '28', '25', '3', 'Minyak Goreng Kemasan Bermerk 2 (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '29', NULL, 'X', 'Gula Pasir', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '30', '29', '1', 'Gula Pasir Kualitas Premium (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' ),
( '31', '29', '2', 'Gula Pasir Lokal (kg)', '2019-05-19 06:27:15', '2019-05-19 06:27:15' );
-- ---------------------------------------------------------


-- Dump data of "jenis_pasar" ------------------------------
INSERT INTO `jenis_pasar`(`id`,`nama`,`created_at`,`updated_at`) VALUES 
( '1', 'Pasar Modern', NULL, NULL ),
( '2', 'Pasar Rakyat', NULL, NULL ),
( '5', 'Pasar Dagang', '2019-05-25 02:06:14', NULL );
-- ---------------------------------------------------------


-- Dump data of "komoditas" --------------------------------
-- ---------------------------------------------------------


-- Dump data of "pasar" ------------------------------------
INSERT INTO `pasar`(`id`,`nama`,`jenis_pasar`,`latitude`,`longitude`,`alamat_pasar`,`created_at`,`updated_at`,`jenis_pasar_id`) VALUES 
( '1', 'Cilegon1', 'Pasar Rakyat', '', '', 'jalan cilegon 1', '2019-05-22 02:52:51', NULL, '1' ),
( '2', 'Cilegon2', 'Pasar Modern', '', '', 'jalan cilegon 2', '2019-05-22 02:53:29', NULL, '2' );
-- ---------------------------------------------------------


-- Dump data of "users" ------------------------------------
INSERT INTO `users`(`id`,`name`,`username`,`password`,`created_at`,`updated_at`,`level`) VALUES 
( '2', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2019-05-26 02:36:46', NULL, 'OPERATOR' );
-- ---------------------------------------------------------


-- Dump data of "settings" ---------------------------------
INSERT INTO `settings`(`key`,`value`) VALUES 
( 'SYARAT_KETENTUAN', '<p>Syarat dan Ketentuan ada di menu backend/settings</p>' ),
( 'PETUNJUK', '<p>Petunjuk Aplikasi ada di menu backend/settings</p>' );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


